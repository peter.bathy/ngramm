import React, {Component} from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {

    state = {
        resultText: '',
        textFieldContent: {
            value: '',
            isValid: true,
            message: '',
        },
    };

    handleChange = event => {

        this.setState({
            textFieldContent: {
                value: event.target.value,
                isValid: true,
                message: '',
            }
        });
    };

    postDataHandler = (event) => {
        event.preventDefault();

        axios.post('http://localhost:8080/api/language', {
            inputText: this.state.textFieldContent.value,
        })
            .then(response => {
                this.setState({resultText: response.data.langCode})
            })
            .catch(error => {
                this.setState({
                    textFieldContent: {
                        value: '',
                        isValid: false,
                        message: error.response.data.errors[0].defaultMessage,
                    },
                })
            });
    };

    render() {
        return (
            <div className="container">
                <h1>Nyelvfelismerés</h1>
                <div className="row">
                    <div className="col-md-10">
                        <div className="form-group">
                            <textarea className={this.state.textFieldContent.isValid ? "form-control" : "form-control is-invalid"} rows="9" id="comment" name="textFieldContent"
                                      onChange={this.handleChange}/>
                            <div className={this.state.textFieldContent.isValid ? "form-control-feedback" : "invalid-feedback"}>{this.state.textFieldContent.message}</div>
                        </div>
                    </div>
                    <div className="col-md-2">
                        <div className="button-wrapper">
                            <button type="button" className="btn btn-primary btn-block"
                                    onClick={this.postDataHandler}>Indítás
                            </button>
                        </div>
                        <div className="text-center">
                            {this.state.resultText &&
                            <div><p>A nyelv kódja:</p>
                                < h1>{this.state.resultText}</h1></div>
                            }
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default App;

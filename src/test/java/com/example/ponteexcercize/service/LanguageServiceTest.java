package com.example.ponteexcercize.service;

import com.example.ponteexcercize.repository.LanguageRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class LanguageServiceTest {

    private LanguageService languageService;
    private LanguageRepository languageRepositoryMock;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    private File en;
    private File hu;
    private File de;
    private File huFile;
    private File enFile1;
    private File enFile2;
    private File deFile;

    @Before
    public void setUp() {
        languageRepositoryMock = mock(LanguageRepository.class);
        languageService = new LanguageService(languageRepositoryMock);
    }

    @Test
    public void testListAllFilesPathListSize() {

        try {
            List<String> paths = getTempFileSystem();

            assertEquals(4, paths.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testListAllFilesSelectedFileIsExist() {

        try {
            List<String> paths = getTempFileSystem();

            String filename = paths.get(1).substring(paths.get(1).length() - 14);
            assertEquals("spiegel.de.txt", filename);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCleanText() {
        String dirtyText = "\t Á.r:v?í-z;t!ű,rő \n tükörfúró\r\nÚjsor";
        String cleanResult = " árvíztűrő tükörfúró újsor";

        assertEquals(cleanResult, languageService.cleanText(dirtyText));
    }

    @Test
    public void testCreateGramsOfText() {
        String textToGrams = "napos lapol";
        Map<String, Integer> expectedGrams = new HashMap<>();

        expectedGrams.put(" na", 1);
        expectedGrams.put("nap", 1);
        expectedGrams.put("apo", 2);
        expectedGrams.put("pos", 1);
        expectedGrams.put("os ", 1);
        expectedGrams.put("s  ", 1);
        expectedGrams.put(" la", 1);
        expectedGrams.put("lap", 1);
        expectedGrams.put("pol", 1);
        expectedGrams.put("ol ", 1);
        expectedGrams.put("l  ", 1);

        assertEquals(expectedGrams,languageService.createGramsOfText(textToGrams));
    }

    @Test
    public void testCalcDistances() {

        Map<String, Integer> languageProfile = new HashMap<>();
        languageProfile.put("first", 1);
        languageProfile.put("second", 2);
        languageProfile.put("third", 3);
        languageProfile.put("fourth", 4);

        Map<String, Integer> analyzedProfile = new HashMap<>();
        analyzedProfile.put("fourth", 1);
        analyzedProfile.put("first", 2);
        analyzedProfile.put("tenth", 3);
        analyzedProfile.put("sixth", 4);

        assertEquals(204,languageService.calcDistances(languageProfile,analyzedProfile));
    }

    @Test
    public void testNormalizeGramFrequency() {
        Map<String, Integer> rawMap = new LinkedHashMap<>();
        rawMap.put("first", 142);
        rawMap.put("second", 102);
        rawMap.put("third", 42);

        Map<String, Integer> normalizedMap = languageService.normalizeGramFrequency(rawMap);

        Map<String, Integer> expectedMapResult = new HashMap<>();
        expectedMapResult.put("first", 1);
        expectedMapResult.put("second", 2);
        expectedMapResult.put("third", 3);

        assertEquals(expectedMapResult, normalizedMap);
    }

    private List<String> getTempFileSystem() throws IOException {
        hu = folder.newFolder("hu");
        en = folder.newFolder("en");
        de = folder.newFolder("de");
        huFile = folder.newFile("hu/index.hu.txt");
        deFile = folder.newFile("de/spiegel.de.txt");
        enFile1 = folder.newFile("en/cnn.com.txt");
        enFile2 = folder.newFile("en/nytimes.com.txt");

        return languageService.listAllFiles(folder.getRoot().toString());
    }
}
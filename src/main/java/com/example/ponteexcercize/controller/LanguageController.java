package com.example.ponteexcercize.controller;

import com.example.ponteexcercize.dto.AnalysationRequest;
import com.example.ponteexcercize.dto.LanguageCodeResponse;
import com.example.ponteexcercize.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/language")
public class LanguageController {
    private LanguageService languageService;

    @Autowired
    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @PostMapping
    public ResponseEntity<LanguageCodeResponse> startLanguageRecognition(@Valid @RequestBody AnalysationRequest textToAnalyze) {

        LanguageCodeResponse languageCodeResponse = languageService.startRecognitionSequence(textToAnalyze);

        return new ResponseEntity<>(languageCodeResponse, HttpStatus.OK);
    }

}

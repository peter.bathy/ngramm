package com.example.ponteexcercize.dto;

import javax.validation.constraints.NotBlank;

public class AnalysationRequest {

    @NotBlank(message = "{inputtext.notblank}")
    private String inputText;

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }
}

package com.example.ponteexcercize.dto;

public class LanguageCodeResponse {

    private String langCode;

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}

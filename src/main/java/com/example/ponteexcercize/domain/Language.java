package com.example.ponteexcercize.domain;

import javax.persistence.*;
import java.util.Map;

@Entity
public class Language {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String langCode;

    @ElementCollection
    @MapKeyColumn(name="grams")
    @Column(name="frequency")
    private Map<String, Integer> grams;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public Map<String, Integer> getGrams() {
        return grams;
    }

    public void setGrams(Map<String, Integer> grams) {
        this.grams = grams;
    }
}

package com.example.ponteexcercize.service;

import com.example.ponteexcercize.domain.Language;
import com.example.ponteexcercize.dto.AnalysationRequest;
import com.example.ponteexcercize.dto.LanguageCodeResponse;
import com.example.ponteexcercize.repository.LanguageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class LanguageService {

    private static final Logger logger = LoggerFactory.getLogger(LanguageService.class);
    private LanguageRepository languageRepository;
    private List<String> filePaths = new ArrayList<>();

    private static final String SOURCEPATH = "src/main/source";
    private static final int LIMIT_OF_GRAMS_PER_LANGUAGE = 100;

    @Autowired
    public LanguageService(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    public void startLearningSequence() {
        logger.info("The learning sequence has started");
        createLanguageProfilesInDatabase();
        logger.info("The learning sequence has finished");
    }

    private void createLanguageProfilesInDatabase() {

        filePaths = listAllFiles(SOURCEPATH);

        Map<String, String> languageSamples = prepareFilesForProfiles(filePaths);

        for (String key : languageSamples.keySet()) {
            Language currentLanguage = new Language();
            currentLanguage.setLangCode(key);
            currentLanguage.setGrams(createGramsOfText(languageSamples.get(key)));

            languageRepository.save(currentLanguage);
        }
    }

    Map<String, Integer> createGramsOfText(String textToGrams) {

        Map<String, Integer> grams = new HashMap<>();
        int gramLength = 3;

        String[] tokens = textToGrams.split(" ");

        for (String token : tokens) {
            token = " " + token;
            for (int i = 0; i < gramLength; i++) {
                token = token + " ";
            }

            for (int i = 0; i < token.length() - gramLength; i++) {
                String gram = token.substring(i, i + gramLength);
                if (grams.containsKey(gram)) {
                    grams.put(gram, grams.get(gram) + 1);
                } else {
                    grams.put(gram, 1);
                }
            }
        }

        grams = sortAndLimitResult(grams);

        return grams;
    }

    private Map<String, Integer> sortAndLimitResult(Map<String, Integer> grams) {

        return grams.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(LIMIT_OF_GRAMS_PER_LANGUAGE)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    List<String> listAllFiles(String directoryName) {
        Path folder = Paths.get(directoryName);
        try (DirectoryStream<Path> contents = Files.newDirectoryStream(folder)) {
            for (Path entry : contents) {
                if (Files.isRegularFile(entry)) {
                    filePaths.add(entry.toAbsolutePath().toString());
                } else if (Files.isDirectory(entry)) {
                    listAllFiles(entry.toAbsolutePath().toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filePaths;
    }

    private Map<String, String> prepareFilesForProfiles(List<String> filePaths) {
        Map<String, String> languageSamples = new HashMap<>();
        String langCode;

        for (String path : filePaths) {

            String scannedText = readFile(path);
            scannedText = cleanText(scannedText);

            int languageIdPlace = path.indexOf("/source/");
            langCode = path.substring(languageIdPlace + 8, languageIdPlace + 10);

            if (!languageSamples.containsKey(langCode)) {
                languageSamples.put(langCode, scannedText);
            } else {
                languageSamples.put(langCode, languageSamples.get(langCode).concat(scannedText));
            }
        }

        return languageSamples;
    }

    private String readFile(String path) {

        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(path), StandardCharsets.UTF_8)) {
            stream.forEach(contentBuilder::append);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    String cleanText(String text) {

        String pattern = "[,.;\\-?!:]";

        text = text
                .replaceAll(pattern, "")
                .replaceAll(System.lineSeparator(), "  ")
                .replaceAll("(\\s{2,})", " ")
                .toLowerCase();

        return text;
    }

    public LanguageCodeResponse startRecognitionSequence(AnalysationRequest textToAnalyze) {

        logger.info("The recognition sequence has tarted with the next text: {}", textToAnalyze.getInputText());

        Map<String, Integer> analyzedTextProfile = createGramsOfText(textToAnalyze.getInputText());

        Map<String, Integer> normalizedTextProfile = normalizeGramFrequency(analyzedTextProfile);

        List<Language> languages = languageRepository.findAll();

        String resultLangCode = findWhichLanguage(normalizedTextProfile, languages);

        LanguageCodeResponse languageCodeResponse = new LanguageCodeResponse();
        languageCodeResponse.setLangCode(resultLangCode);

        return languageCodeResponse;
    }

    private String findWhichLanguage(Map<String, Integer> analyzedTextProfile, List<Language> languages) {

        String result = "";
        int distanceSum = Integer.MAX_VALUE;

        for (Language language: languages) {

            Map<String, Integer> languageProfile = normalizeGramFrequency(language.getGrams());

            int currentDistenceSum = calcDistances(analyzedTextProfile, languageProfile);
            if (currentDistenceSum < distanceSum) {
                distanceSum = currentDistenceSum;
                result = language.getLangCode();
            }
        }

        return result;
    }

    int calcDistances(Map<String, Integer> analyzedTextProfile, Map<String, Integer> languageProfile) {

        int distanceValue = 0;

        for (String key : analyzedTextProfile.keySet()) {
            if (languageProfile.containsKey(key)) {
                distanceValue += Math.abs(languageProfile.get(key) - analyzedTextProfile.get(key));
            } else {
                distanceValue += LIMIT_OF_GRAMS_PER_LANGUAGE;
            }
        }

        return distanceValue;
    }

    Map<String, Integer> normalizeGramFrequency(Map<String, Integer> analyzedTextProfile) {

        int frequencyOrder = 1;

        for (String key:analyzedTextProfile.keySet()) {
            analyzedTextProfile.put(key, frequencyOrder++);
        }

        return analyzedTextProfile;
    }
}

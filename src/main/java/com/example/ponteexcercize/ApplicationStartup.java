package com.example.ponteexcercize;

import com.example.ponteexcercize.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private LanguageService languageService;

    @Autowired
    public ApplicationStartup(LanguageService languageService) {
        this.languageService = languageService;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        languageService.startLearningSequence();
    }
}

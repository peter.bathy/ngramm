package com.example.ponteexcercize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PonteExcercizeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PonteExcercizeApplication.class, args);
    }

}


﻿## N-gram language identifier - Ponte.hu excercise

### Getting started

1. Start back-end:
    ```
	  mvn spring-boot:run
    ```
2. Start front-end:
    ```
	  cd front-end
    npm start
    ```
